module.exports = {
    entry: [
        './src/index.html'
    ],
    output: {
        path: __dirname + '/dist',
    },
    module: {
        rules: [{
            test: /\.(html)$/,
            use: ['file-loader?name=[name].[ext]', 'extract-loader', 'html-loader']
        }]
    }
}