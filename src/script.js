import $ from 'jquery'

import {
    MDCTopAppBar
} from '@material/top-app-bar';

import {
    MDCRipple
} from '@material/ripple';
import {
    MDCChipSet
} from '@material/chips';
import {
    MDCChip
} from '@material/chips';
import {
    MDCTextField
} from '@material/textfield';
import {
    MDCList
} from '@material/list';

$(() => {


    // Initializing the navbar
    MDCTopAppBar.attachTo($('#navbar')[0])




    // Initializing buttons
    $('button, i').mouseup(e => $(e.target).closest('button').removeClass('mdc-ripple-upgraded--background-focused'))
    $('button').each((i, el) => MDCRipple.attachTo(el))




    // Initializing alert type toggles
    $('#alert-types>button').click((e) => {
        $(e.target).closest('button').toggleClass('active inactive')
    })




    // Initialzing the label filters
    let labels = new MDCChipSet($('#filters .labels .list')[0])

    labels.listen('MDCChip:removal', (e) => {
        $('#filters .labels .list')[0].removeChild(e.detail.root)
        updateLabels()
    })

    $('#filter-field__wrapper input').keyup((e) => {
        if (e.key == 'Enter' || e.keyCode == 13) {
            let filter = $(e.target).val()

            if (!filter) return

            let chip = $('<div tabindex="0"/>')
                .append($('<div />').text(filter))
                .append($('<i class="material-icons" tabindex="0" role="button" />')
                    .text('cancel')
                    .click(e => removeLabel(e)))

            $('#filters .labels .list').append(chip)
            labels.addChip(chip[0])
            $(e.target).val('')

            updateLabels()
        }
    })

    $('#filters .labels .list > div i').click(e => removeLabel(e))




    // Initializing date range
    $('.mdc-text-field').each((i, el) => MDCTextField.attachTo(el))



    //Initiating alarm list
    const alarms_list = new MDCList($('#alarms-list>ul')[0]);




    //Initiating all alarms
    const all_alarms = new MDCList($('#all-alarms>ul')[0])
})

function removeLabel(e) {
    let chip = new MDCChip($(e.target).parent()[0])
    chip.beginExit()
}

function updateLabels() {
    let chipList = $('#filters .labels .list')
    chipList.css('display', chipList.contents().length - 1 ? 'inline-block' : 'none')
}